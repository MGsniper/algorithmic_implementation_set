function bubbleSort(arr) {
    let loopTimes = 0  // 循环计数器
    let steps = 0 // 步数
    let last = arr.length - 1;
    for (let i = 0, len = arr.length; i < len; i++) {
        let finished = true // flag
        let pos = 0;
        for (let j = 0, len = last; j < len; j++) {
            if (arr[j] > arr[j + 1]) {
                finished = false;
                temp = arr[j + 1]
                arr[j + 1] = arr[j]
                arr[j] = temp
                pos = j
            }
            steps++;
        }
        last = pos
        loopTimes++;
        if (finished)
            break
    }
    console.log(steps)
    return { loopTimes, steps }
}

let arr = [6, 4, 3, 5, 2, 1, 9, 10, 11, 12, 14, 15]
let res = bubbleSort(arr)

console.log('the Array after sorting:' + arr)
console.log('steps:' + res.steps)
console.log('loopTimes:' + res.loopTimes)
