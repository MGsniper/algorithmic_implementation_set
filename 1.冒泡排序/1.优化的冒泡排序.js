function bubbleSort(arr) {
    let loopTimes = 0
    for (let i = 0, len = arr.length; i < len; i++) {
        let finished = true
        for (let j = 0, len = arr.length - i - 1; j < len; j++) {
            if (arr[j] > arr[j + 1]) {
                finished = false;
                temp = arr[j + 1]
                arr[j + 1] = arr[j]
                arr[j] = temp
            }
        }
        loopTimes++;
        if (finished)
            break
    }
    return loopTimes
}

let arr = [2, 3, 9, 4, 5]

console.log('loopTimes:' + bubbleSort(arr))
console.log('the Array after sorting:' + arr)